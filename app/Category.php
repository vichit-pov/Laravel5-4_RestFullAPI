<?php

namespace App;

use App\Product;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name',
        'description',
    ];

     protected $hidden = [
        'created_at',
    ];

    // protected $table = 'cateory_product';

    public function products()
    {
    	//Many to Many Relationship
        return $this->belongsToMany(Product::class);

    }
}
