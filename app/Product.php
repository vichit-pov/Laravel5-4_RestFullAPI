<?php

namespace App;

use App\Seller;
use App\Category;
use App\Transaction;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    const AVAILABLE_PRODUCT = 'available';
    const UNAVAILABLE_PRODUCT = 'unavaialbe';

    protected $fillable = [
        'name',
        'description',
        'quantity',
        'status',
        'image',
        'seller_id',
    ];


    public function isAvailable()
    {
        return $this->status == Product::AVAILABLE_PRODUCT;
    }

    public function seller()
    {

        return $this->belongsTo(Seller::class);

    }

    public function transactions()
    {
        //One to Many 
        return $this->hasMany(Transaction::class);

    }


    public function categories()
    {
         //Many to Many Relationship
        return $this->belongsToMany(Category::class);
    }
}
